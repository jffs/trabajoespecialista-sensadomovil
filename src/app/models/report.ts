export class IReport {
    id: number;
    location: ILocation;
    photo: string;
    description: string;
    type: number;
    fecha: Date;
}
export interface ILocation {
    latitude: number,
    longitude: number
}