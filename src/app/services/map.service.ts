import { Injectable } from '@angular/core';
import { IReport } from '../models/report';
import {LatLng as LatitudeLongitude} from "@molteni/coordinate-utils/dist/LatLng";
import { RandomCoordinateUtils } from '@molteni/coordinate-utils/dist/random-coordinate-utils';
@Injectable({
  providedIn: 'root'
})
export class MapService {
  reports: IReport[] = [];
  constructor() { }
  getReports(latitude, longitude) {
    for (let i = 0; i < 25; i++) {
      const location = this.generateCoordinates(latitude, longitude,  Math.random() * 200);
      let report = new IReport();
      report.id = i;
      report.location = location;
      report.type = this.getWaterQuality();
      report.fecha = new Date();
      this.setPhotoAndDescription(report);
      this.reports.push(report);
    }
    return this.reports;
  }
  private  generateCoordinates(lat, long, mts) : LatitudeLongitude {
    return RandomCoordinateUtils.randomCoordinateFromPositionWithExplicitLatLng(lat, long, mts/1000); //tengo que pasarle los kilometros, por eso divido los metros en mil
  };
  private getWaterQuality(){
    return Math.floor(Math.random() * 3); // 0 1 2 
  }
  private setPhotoAndDescription(report: IReport){
    switch (report.type) {
      case 0: report.photo = "https://thumbs-prod.si-cdn.com/gPxZYxgoz_pXmcNY1_pZygBKlqw=/800x600/filters:no_upscale()/https://public-media.si-cdn.com/filer/01/fb/01fb1828-7c3e-4a54-8228-a9dc21fbcaf8/waterglass_edit.jpg";
      report.description = "Buen estado en general"; break;
      case 1: report.photo="https://www.godwinplumbing.com/wp-content/uploads/2020/01/why-does-my-water-smell.jpg";
      report.description="poca presión"; break;
      case 2: report.photo="https://i.insider.com/5c8c1ee469bdec5f3324a6a5?width=1800&format=jpeg&auto=webp";
      report.description = "Color marrón !!";
      break;
    }
  }
}
