import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { IReport } from 'src/app/models/report';

@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.scss'],
})
export class ReportDetailComponent implements OnInit {
  @Input() report: IReport;
  constructor(private modalController: ModalController) { }

  ngOnInit() {}
  dismiss() {
    this.modalController.dismiss();
  }
  getWaterQualityIcon(){
    switch (this.report.type) {
      case 0: return './assets/icon/water-good.svg';
      case 1: return './assets/icon/water-medium.svg';
      case 2: return './assets/icon/water-danger.svg';
    }
  }
}
