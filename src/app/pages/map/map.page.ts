import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import {LatLng as LatitudeLongitude} from "@molteni/coordinate-utils/dist/LatLng";
import { RandomCoordinateUtils } from '@molteni/coordinate-utils/dist/random-coordinate-utils';
import { IReport } from 'src/app/models/report';
import { MapService } from 'src/app/services/map.service';
import { ReportFormPage } from '../report-form/report-form.page';
import { ReportDetailComponent } from './components/report-detail/report-detail.component';
import { CupertinoPane, CupertinoSettings } from 'cupertino-pane';

declare var google;

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  @ViewChild('map',  {static: false}) mapElement: ElementRef;
  map: any;
  lat: number;
  long: number; 
  selectedReport: any;
  pane: any;
  constructor( 
    private geolocation: Geolocation,
    public zone: NgZone,
    private modalController: ModalController,
    public _map: MapService,
    private routerOutlet: IonRouterOutlet
    ) { }

  ngOnInit() {
    this.loadMap();
  }
  loadMap() {
    
    //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
    this.geolocation.getCurrentPosition().then((resp) => {
      this.long = resp.coords.longitude;
      this.lat = resp.coords.latitude;
      const latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      const mapOptions = {
        center: latLng,
        zoom: 20,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        clickableIcons: false,
        styles: [
          {
            featureType: "poi.business",
            stylers: [{ visibility: "off" }],
          },
          {
            featureType: "transit",
            elementType: "labels.icon",
            stylers: [{ visibility: "off" }],
          },
        ]
      } 
      
      //CUANDO TENEMOS LAS COORDENADAS SIMPLEMENTE NECESITAMOS PASAR AL MAPA DE GOOGLE TODOS LOS PARAMETROS.
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions); 
      this.showMyMarker(latLng);
      this.getCloseReports();
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }
  async openReportModal(){
    const modal = await this.modalController.create({
        component: ReportFormPage,
        //cssClass: 'small-modal',
        backdropDismiss: true,
        swipeToClose: true,
        presentingElement: this.routerOutlet.nativeEl
      });
      modal.onDidDismiss().then((() => {
        const center = new google.maps.LatLng(this.lat, this.long);
        this.map.setZoom(20);
        this.map.setCenter(center);
            }));
      return await modal.present();
}
  showMyMarker(latLng) {
    const ICON_MARKER = {
      url: './assets/icon/usuario.svg',
      scaledSize: new google.maps.Size(40, 40),
  };
  const marker = new google.maps.Marker({
      position: latLng,
      map: this.map,
      animation: google.maps.Animation.DROP,
      icon: ICON_MARKER,
  });

  }
  getCloseReports() {
    console.log('> Close Users');
    const reports = this._map.getReports(this.lat, this.long);
    reports.forEach((report) => {
      this.addMarkersToMap(report);
    });
}
 generateCoordinates(lat, long, mts) : LatitudeLongitude {
    return RandomCoordinateUtils.randomCoordinateFromPositionWithExplicitLatLng(lat, long, mts/1000); //tengo que pasarle los kilometros, por eso divido los metros en mil
  };
  //TODO change location for Report
  addMarkersToMap(report: IReport){
      const ICON_MARKER = {
          url: this.getWaterQualityIcon(report.type),
          scaledSize: new google.maps.Size(50, 50)
      };
      const {location} = report;
      const position = new google.maps.LatLng(location.latitude, location.longitude);
      const eventMarker = new google.maps.Marker({
          position,
          animation: google.maps.Animation.DROP,
          icon: ICON_MARKER,
          disableAutoPan: true
      });
      //this.usersMarker.push(eventMarker);
      eventMarker.setMap(this.map);
      google.maps.event.addListener(eventMarker, 'click', ($event) => {
          console.log('SE ABRIO', report);
          this.selectedReport = report;
         // this.map.setCenter(eventMarker.getPosition());
          //this.map.setZoom(20);
          let settings: CupertinoSettings = { backdrop: true, cssClass: 'user-wrapper', bottomClose: true,
          bottomOffset: 150,
          buttonDestroy: true,
          showDraggable: true,
          draggableOver: true,
          onDidDismiss: (e: any) => {
              console.log('dismiss panel');
          },
          onDrag:(e: any) => {
            console.log('drag panel');
        }, 
    };
          if (this.pane){
            this.pane.present({animate: true});
          } else {
            this.pane = new CupertinoPane('.cupertino-pane', settings);
            console.log(this.pane);
            this.pane.present({animate: true});
          }

      }); 
  }
  getWaterQualityIcon(type: number){
    switch (type) {
      case 0: return './assets/icon/water-good.svg';
      case 1: return './assets/icon/water-medium.svg';
      case 2: return './assets/icon/water-danger.svg';
    }
  }
  async presentModal(report: any){
    const modal = await this.modalController.create({
        component: ReportDetailComponent,
        cssClass: 'small-modal',
        backdropDismiss: true,
        swipeToClose: true,
        componentProps: {
           report,
          }
      });
      modal.onDidDismiss().then((() => {
        const center = new google.maps.LatLng(this.lat, this.long);
        this.map.setZoom(20);
        this.map.setCenter(center);
            }));
      return await modal.present();
}
}
