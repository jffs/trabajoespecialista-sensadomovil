import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReportFormPage } from './report-form.page';

describe('ReportFormPage', () => {
  let component: ReportFormPage;
  let fixture: ComponentFixture<ReportFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportFormPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
